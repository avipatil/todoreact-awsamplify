import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { baseUrl } from "../constants";
import axios from "axios";
import "../App.css";
//import nodemailer from 'nodemailer';

const Signup = () => {
  const [email, setEmail] = useState("");
  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();


  // const Mailtodo = () => {

  //   //var nodemailer = require('nodemailer');

  //   var transporter = nodemailer.createTransport({
  //     service: 'gmail',
  //     auth: {
  //       user: 'Avinashpatil92972@gmail.com',
  //       pass: '@Avinash9297'
  //     }
  //   });

  //   var mailOptions = {
  //     from: 'Avinashpatil92972@gmail.com',
  //     to: 'Avinashpatil9297@gmail.com',
  //     subject: 'Welcome mail!',
  //     text: 'That was easy! Welcome to ToDo App Enjoy. This is system generated mail plz do not rpy'
  //   };

  //   transporter.sendMail(mailOptions, function (error, info) {
  //     if (error) {
  //       console.log(error);
  //     } else {
  //       console.log('Email sent: ' + info.response);
  //     }
  //   });
  // };


  const registerUser = (e) => {

    e.preventDefault(); // If we remove this Registration will not take place. This function will prevent on onClick.
    if (firstname.length === 0) {
      alert("please enter firstname");
    } else if (lastname.length === 0){
      alert("please enter lastname")
    } else if (email.length === 0) {
      alert("please enter email");
    } else if (password.length === 0) {
      alert("please enter password");
    }  else {

      const data = {
        ACTION:"register",
        USER:{}
      };
      data.USER.email = email;
      data.USER.password = password;
      data.USER.firstname = firstname;
      data.USER.lastname = lastname;
      axios.post(baseUrl + "/user", data).then((response) => {
        if (response.data.statusCode === 200) {
          //Mailtodo();
          alert("Registered Succesfull");
          history.push("/signin");
        } else {
          alert("Failed to register try again..");
          history.push("/signup");
        }
      });
    }
  };

  return (
    <div className="parent">
      <div id="loginform" className="App">
        <h1>Register</h1>
        <form className="form">
          <div className="my-input-group">
            <label>First Name</label>
            <input
              type="text"
              placeholder="Enter your firstname"
              onChange={(event) => {
                setFirstName(event.target.value);
              }}
            />
          </div>
          <div className="my-input-group">
            <label>Last Name</label>
            <input
              type="text"
              placeholder="Enter your lastname"
              onChange={(event) => {
                setLastName(event.target.value);
              }}
            />
          </div>
          <div className="my-input-group">
            <label>Email</label>
            <input
              type="text"
              placeholder="Enter your firstname"
              onChange={(event) => {
                setEmail(event.target.value);
              }}
            />
          </div>
          <div className="my-input-group my-input-group-register">
            <label>Password</label>
            <input
              type="password"
              placeholder="Enter your password"
              onChange={(event) => {
                setPassword(event.target.value);
              }}
            />
          </div>
          <button className="primary login-btn" onClick={registerUser}>
            Register
          </button>
        </form>
      </div>
    </div>
  );
};

export default Signup;
