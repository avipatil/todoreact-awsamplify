import React , {useContext} from 'react';
import axios from "axios";
import Icon from "react-crud-icons";
import "../../node_modules/react-crud-icons/dist/css/react-crud-icons.css";

import dateFormat from 'dateformat';
import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { baseUrl } from "../constants";
import UserContext from '../Session';
import "./List.css";


const List = () => {
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState('all');
  const { value, setValue } = useContext(UserContext);
  const history = useHistory();

  useEffect(() => {
    getTodos();
  }, [filter]);

  const getTodos = () => {
    const data = {
      ACTION:"get",
      TASK:{
        id:value.id.S
      }
    }
    axios
      .post(baseUrl + "/save", data)
      .then((response) => {
        let data = JSON.parse(response.data.body);
       
        let sortedData = sortTasks(data.Items);
        setTodos(sortedData);
      });
  };


  const deleteTodo = (id) => {
    const data = {
      ACTION:"delete",
      TASK:{
        id:id
      }
    }
    axios
      .post(baseUrl + "/save", data)
      .then((response) => {
        if(response.data.statusCode === 200){
          getTodos();
          alert('task deleted successfully');
          history.push('/home');
        }
      });
  };


  const archiveTodo = (id) => {
    const data = {
      ACTION:"archive",
      TASK:{
        id:id
      }
    }
    axios
      .post(baseUrl + "/save", data)
      .then((response) => {
        if(response.data.statusCode === 200){
          getTodos();
          alert('task archived successfully');
          history.push('/home');
        }
      });
  };


  const completeTodo = (id) => {
    
    const data = {
      ACTION:"complete",
      TASK:{
        id:id
      }
    }
    axios
      .post(baseUrl + "/save", data)
      .then((response) => {
        if(response.data.statusCode === 200){
          getTodos();
          alert('task completed successfully');
          history.push('/home');
        }
      });
  };

  function sortTasks(array){
    if(filter === 'complete'){
      array = array.filter((ele)=> ele.isCompleted.BOOL);
      
    }

    if(filter === 'incomplete' ){
      array = array.filter((ele)=> !ele.isCompleted.BOOL && !ele.isArchived.BOOL);
    }

    if(filter === 'archive'){
      array = array.filter((ele)=> ele.isArchived.BOOL);
    }

    if(filter === 'all'){
      array = array.filter((ele)=> !ele.isCompleted.BOOL && !ele.isArchived.BOOL);
    }

    return array.sort(function(a,b){
      return new Date(parseInt(b.updatedAt.S)) - new Date(parseInt(a.updatedAt.S));
    });
  }



  return (
    <div className="container">
      <p className="title_title__mJ8OQ">TODO List</p>
      <div className="app_app__wrapper__">
        <div className="app_appHeader__">
          <button type="button" className="button_button__zbfSX button_button--primary__09xDJ" onClick={()=> history.push('/addtask')} >
            Add Task
          </button>
          <select onChange={(e) => setFilter(e.target.value)} id="status" className="button_button__abc  button_button__select__">
            <option value="all" className='option-color'>All</option>
            <option value="incomplete" className='option-color'>Incomplete</option>
            <option value="complete" className='option-color'>Completed</option>
            <option value="archive" className='option-color'>archived</option>
          </select>
        </div>
        <div className="app_content__wrapper__" style={{opacity: 1,  transform: "none"}}>
           
          {
            todos.map((t)=>{
              return <div>
                      <div className="todoItem_item__fnR7B" style={{opacity: 1, transform: "none"}}>
                        <div className="todoItem_todoDetails__zH8Q3">
                          <div className="todoItem_texts__-ozZm">
                            <div className='todo-chck-container'>
                              <div >
                                <input className='todo-chck' onChange={()=>completeTodo(t.id.S)} type="checkbox" />
                              </div>
                              <div>
                                <p className="todoItem_todoText__j68oh">{t.text.S}</p>
                                <p className="todoItem_time__08Ivc">{dateFormat(new Date(parseInt(t.updatedAt.S)), "mmm dd,yyyy h:MM:ss tt")}</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className='icon-todo'>
                          <Icon
                            name = "export"
                            tooltip = ""
                            theme = "light"
                            size = "medium"
                            onClick={()=> archiveTodo(t.id.S)}
                          />
                          <Icon
                            name = "edit"
                            tooltip = ""
                            theme = "light"
                            size = "medium"
                            onClick={()=>history.push('/edit-todo', { params: t })}
                          />
                          <Icon
                            name = "delete"
                            tooltip = ""
                            theme = "light"
                            size = "medium"
                            onClick={() => deleteTodo(t.id.S)}
                          />
                        </div>
                      </div>
                     </div>
            })
          }
            
        </div>
      </div>
    </div>
  );
};

export default List;
