import React, { useState, useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom";
import { baseUrl } from "../constants";
import axios from "axios";
import '../App.css'

const EditTodo = () => {
    const [text, setText] = useState("");
    const history = useHistory();
    const location = useLocation();
    const myparam = location.state.params;

    useEffect(()=>{
        getText()
    },[]);

    const getText = () => {
        setText(myparam.text.S)
    }


    const editTodo = (e) => {
      e.preventDefault();
      if (text.length === 0) {
        alert("please enter text");
      } else {
        const data = {
          ACTION:"update",
          TASK:{}
        };
        data.TASK.text = text;
        data.TASK.id = myparam.id.S;

        axios.post(baseUrl + "/save", data).then((response) => {
          if (response.data.statusCode === 200) {
            alert("task edited Succesfull");
            history.push("/home");
          } else {
            alert("failed to edit");
          }
        });
      }
    };
  
  
    return (
      <div className="parent">
        <div id="loginform" className="App">
          <h1>EDIT TODO</h1>
          <form className="form">
            <div>
              <div className="my-input-group">
                <label>Text</label>
                <input
                  type="text"
                  placeholder="Enter text"
                  onChange={(event) => {
                    setText(event.target.value);
                  }}
                  value = {text}
                />
              </div>
              <button className="primary login-btn" onClick={editTodo}>
                EDIT
              </button>
            </div>
          </form>
        </div>
      </div>
    );
};

export default EditTodo;