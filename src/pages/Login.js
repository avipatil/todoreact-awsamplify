import axios from "axios";
import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import "../App.css";
import UserContext from '../Session'
import { baseUrl } from "../constants";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { value, setValue } = useContext(UserContext)
  const history = useHistory();

  const signinUser = (e) => {
    e.preventDefault();
    if (email.length === 0) {
      alert("please enter email");
    } else if (password.length === 0) {
      alert("please enter password");
    } else {
      const data = {
        ACTION : "",
        USER: {}
      };
      data.ACTION = "login"
      data.USER.email = email;
      data.USER.password = password;

      axios.post(baseUrl + "/user", data).then((response) => {
        if (response.data.statusCode === 200) {
          const data = JSON.parse(response.data.body)
          alert("Login Succesfull");
          setValue(data.Items[0]);
          history.push("/home");
        } else {
          alert("Login Filed, Email or password incorrect");
        }
      });
    }
  };


  return (
    <div className="parent">
      <div id="loginform" className="App">
        <h1>Login</h1>
        <form className="form">
          <div>
            <div className="my-input-group">
              <label>Email</label>
              <input
                type="text"
                placeholder="Enter your username"
                onChange={(event) => {
                  setEmail(event.target.value);
                }}
              />
            </div>
            <div className="my-input-group my-input-group-last">
              <label>Password</label>
              <input
                type="password"
                placeholder="Enter your password"
                onChange={(event) => {
                  setPassword(event.target.value);
                }}
              />
            </div>
            <button className="primary login-btn" onClick={signinUser}>
              Login
            </button>
            <div className="link1">
              <Link to="/signup">don't have an account</Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
