import React, { useState, useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { baseUrl } from "../constants";
import UserContext from "../Session";
import axios from "axios";
import '../App.css'

const AddTodo = () => {
    const [text, setText] = useState("");
    const { value, setValue } = useContext(UserContext);
    const history = useHistory();
  
    const saveTodo = (e) => {
      e.preventDefault(); // It will prevent on onClick that saveTodo function will run. if we remove this saveTodo will not run 
      if (text.length === 0) {
        alert("please enter text");
      } else {
        const data = {
          ACTION:"insert",
          TASK:{}
        };
        data.TASK.text = text;
        data.TASK.userId = value.id.S;

        axios.post(baseUrl + "/save", data).then((response) => {
          if (response.data.statusCode === 200) {
            alert("task added Succesfull");
            history.push("/home");
          } else {
            alert("failed to add");
          }
        });
      }
    };
  
  
    return (
      <div className="parent">
        <div id="loginform" className="App">
          <h1>Add TODO</h1>
          <form className="form">
            <div>
              <div className="my-input-group">
                <label>Text</label>
                <input
                  type="text"
                  placeholder="Enter text"
                  onChange={(event) => {
                    setText(event.target.value);
                  }}
                />
              </div>
              <button className="primary login-btn" onClick={saveTodo}>
                Add
              </button>
            </div>
          </form>
        </div>
      </div>
    );
};

export default AddTodo;