import React  from 'react';
import List from './List';
import Header from '../components/Header';

const Home = () => {
    return (
        <div>
            <Header />
            <List />
        </div>
    );
};

export default Home;