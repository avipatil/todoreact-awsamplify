import "./App.css";
import React,{useState} from 'react';
import Login from "./pages/Login";
import Signup from "./pages/Register";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import UserContext from "./Session";
import AddTodo from "./pages/AddTodo";
import EditTodo from "./pages/EditTodo";

function App() {
  const [value, setValue] = useState('')
  return (
    <div >
        <BrowserRouter>
          <Switch>
            <UserContext.Provider value={{value,setValue}}>
              <Route exact path='/' component={Login} />
              <Route path='/signin' component={Login} />
              <Route path='/signup' component={Signup} />
              <Route path='/home' component={Home} />
              <Route path='/addtask' component={AddTodo} />
              <Route path='/edit-todo' component={EditTodo} />
            </UserContext.Provider> 
          </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
